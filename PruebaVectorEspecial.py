'''
Created on 2/09/2021

@author: Herrera

Operaciones: 
    
        [HECHO] 1) Crear
        [HECHO] 2) Llenar
        3) Obtener posicion inicio
        4) Obtener posicion fin
        5) Obtener cantidad elementos    
        [HECHO] 6) Mostrar todo los elementos
        7) Mostrar elemento del inicio
        8) Mostrar elemento del fin
        9) Aumentar tama\xf3o del arreglo
        10) Disminuir tama\xf3o del arreglo    
        11) Insertar elemento en posicion especifica
        12) Eliminar elemento de posicion especifica
        13) Invertir el vector

'''

import random

class VectorEspecial:
    '''Clase para crear Vectores con funcionalidades especiales'''
    #1) Crear
    def __init__(self, tam):
        self.numeros = []
        self.tam = tam
    
    #2) Llenar
    def llenarVectorAutomatizado(self):
        for i in range(self.tam):
            i=i
            self.numeros.append(int(random.randrange(1, 100, 1)))
    
    def llenarVectorTeclado(self):
        print("Ingresa numeros para llenar el vector")
        for i in range(self.tam):
            n = input("Ingresa calificion " + (i+1) + ": ")
            self.numeros.append(int(n))
    
    def mostrarVector(self):
        for i in range(len(self.numeros)):
            print(self.numeros[i], end=" ")
        print()
            
    def obtenerPosicionInicio(self):
        print(f"Posicion de inicio: {0}")
        
    def obtenerPosicionFin(self):
        print(f"Pisicion de fin: {(len(self.numeros))-1}")
        
    def obtenerCantidadElementos(self):
        print(f"Cantidad de elementos; {len(self.numeros)}")
        
    def obtenerElementoInicio(self):
        print(f"Primer elemento: {self.numeros[0]}")
        
    def obtenerElementoFinal(self):
        print(f"Ultimo elemento: {self.numeros[len(self.numeros)-1]}" )
    
    def aumentarTamArreglo(self, nuevoTam):
        numAgregar = nuevoTam - len(self.numeros)
        for i in range(numAgregar):
            self.numeros.append(int(0))
        return self.numeros
    
    def disminuirTamArreglo(self, nuevoTam):
        nuevoVector = []
        for i in range(nuevoTam):
            nuevoVector.append(self.numeros[i])
        return nuevoVector
    
    def insertarElementoPosicion(self, posicion, elemento):
        self.numeros[posicion] = elemento
        return self.numeros
        
    def eliminarElemento(self, posicion):
        nuevoVector = [];
        for i in range(len(self.numeros)):
            if (i!=posicion):
                nuevoVector.append(self.numeros[i])
        return nuevoVector;
                
        

    def invertirVector(self):
        numerosInvertidos = self.numeros[::-1]
        return numerosInvertidos
            
    

tam = int(input("Introduce el tamano del vector: "))
v1 = VectorEspecial(tam)
v1.llenarVectorAutomatizado();

opcion = 0

while (opcion != 12):
    print()
    print("Elije una de las siguientes opciones");
    print("1) Obtener pocicion de inicio");
    print("2) Obtener pocicion final ");
    print("3) Obtener cantidad de elementos");
    print("4) Mostrar todos los elementos");
    print("5) Mostrar elemento de inicio");
    print("6) Mostrar elemento de fin");
    print("7) Aumentar tamano del arreglo");
    print("8) disminuir tamano del arreglo");
    print("9) Insertar elemento en posicion especifica");
    print("10) Eliminar elemento de posicion especifica");
    print("11) Invertir vector");
    
    opcion = int(input("Introduce opcion: "))
    
    if (opcion == 1):
        v1.obtenerPosicionInicio()
        
    elif (opcion == 2):
        v1.obtenerPosicionFin()
        
    elif (opcion == 3):
        v1.obtenerCantidadElementos()
        
    elif (opcion == 4):
        v1.mostrarVector()
        
    elif (opcion == 5):
        v1.obtenerElementoInicio()
        
    elif (opcion == 6):
        v1.obtenerElementoFinal()
        
    elif (opcion == 7):
        nuevoTam = int(input("Introduce nuevo tamano (mayor): "))
        if (nuevoTam > len(v1.numeros)):
            v1.numeros = v1.aumentarTamArreglo(nuevoTam)
            print(">>Tamano del vector aumentado")
        else:
            print("El numero debe ser mayor")
        
    elif (opcion == 8):
        nuevoTam = int(input("Introduce nuevo tamano (menor): "))
        if (nuevoTam<len(v1.numeros) and nuevoTam>0):
            v1.numeros = v1.disminuirTamArreglo(nuevoTam)
            print(">> Tamano del vector disminuido")
        else:
            print("El numero debe ser menor")
        
    elif (opcion == 9):
        posicion = int(input("Introduce pocicion a insertar: "))
        if (posicion>=0 and posicion <= len(v1.numeros)):
            elemento = int(input("Introduce elemento a insertar: "))
            v1.numeros = v1.insertarElementoPosicion(posicion-1, elemento)
            print(">> Elemento insertado")
        else:
            print("La posicion no existe")
        
    elif (opcion == 10):
        posicion = int(input("Introduce pocicion a eliminar: "))
        if (posicion>=0 and posicion <= len(v1.numeros)):
            v1.numeros = v1.eliminarElemento(posicion-1)
            print(">> Elemento eliminado")
        else:
            print("la pocicion no existe")
        
    elif (opcion == 11):
        v1.numeros = v1.invertirVector()
        print(">> Vector invertido")
        
    elif (opcion == 12):
        print("Saliendo . . .")
    else:
        print("Opcion incorrecta")